﻿using System;
using System.Collections.Generic;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

namespace OpenTerra.Engine
{
	/// <summary>
	/// Represents a coordinate pair for one Chunk
	/// </summary>
	public struct ChunkCoordinate
	{
		public int ChunkX { get; set; }

		public int ChunkY { get; set; }

		public ChunkCoordinate(int x, int y) : this()
		{
			this.ChunkX = x;
			this.ChunkY = y;
		}
	}

	/// <summary>
	/// A chunk stores a fixed-size matrix of Tiles along with optional TileEntities
	/// 
	/// The generic type <typeparamref>T</typeparamref> represents the required superclass
	/// of all TileEntities to be stored with this chunk's tiles.
	/// </summary>
	public class Chunk<T> where T : TileEntity
	{
		private Tile[,] _worldMatrix;

		public ChunkCoordinate Location { get; private set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="OpenTerra.Engine.Chunk`1"/> class.
		/// </summary>
		public Chunk(ChunkCoordinate location)
		{
			this.Location = location;
			this._worldMatrix = new Tile[World.CHUNK_WIDTH_IN_TILES, World.CHUNK_HEIGHT_IN_TILES];
		}

		public bool IsCoordinateValid(int x, int y)
		{
			return x >= 0 && y >= 0 && x < World.CHUNK_WIDTH_IN_TILES && y < World.CHUNK_HEIGHT_IN_TILES;
		}

		public Tile this[int x, int y]
		{
			get
			{
				if (!IsCoordinateValid(x, y))
					throw new ArgumentOutOfRangeException(string.Format("Coordinate ({0}, {1}) is not valid for chunk at ({2}, {3})", x, y, this.Location.ChunkX, this.Location.ChunkY), (Exception) null);
				return this._worldMatrix[x, y];
			}
			set
			{
				if (!IsCoordinateValid(x, y))
					throw new ArgumentOutOfRangeException(string.Format("Coordinate ({0}, {1}) is not valid for chunk at ({2}, {3})", x, y, this.Location.ChunkX, this.Location.ChunkY), (Exception) null);
				this._worldMatrix[x, y] = value;
			}
		}
	}

	public interface ILayer
	{
		Type TileEntityType { get; }
	}

	public class Layer<T> : ILayer where T : TileEntity
	{
		private Dictionary<ChunkCoordinate, Chunk<T>> _chunks;

		public Layer()
		{
			this._chunks = new Dictionary<ChunkCoordinate, Chunk<T>>();
		}

		public Type TileEntityType
		{
			get
			{
				return typeof(T);
			}
		}

		public bool IsChunkCoordinateValid(ChunkCoordinate location)
		{
			return this._chunks.ContainsKey(location);
		}

		public Tile this[int x, int y]
		{
			get
			{
				ChunkCoordinate cCoord = World.TileToChunkCoordinate(x, y);
				if (!IsChunkCoordinateValid(cCoord))
					throw new ArgumentOutOfRangeException(string.Format("Coordinate ({0}, {1}) is not a valid tile location", x, y), (Exception) null);
				return this._chunks[cCoord][x % World.CHUNK_WIDTH_IN_TILES, y % World.CHUNK_HEIGHT_IN_TILES];
			}
			set
			{
				ChunkCoordinate cCoord = World.TileToChunkCoordinate(x, y);
				if (!IsChunkCoordinateValid(cCoord))
					throw new ArgumentOutOfRangeException(string.Format("Coordinate ({0}, {1}) is not a valid tile location", x, y), (Exception) null);
				this._chunks[cCoord][x % World.CHUNK_WIDTH_IN_TILES, y % World.CHUNK_HEIGHT_IN_TILES] = value;
			}
		}
	}

	public class World
	{
		public const int CHUNK_WIDTH_IN_TILES = 64;
		public const int CHUNK_HEIGHT_IN_TILES = 64;
		public const int CHUNK_TILE_SIZE_X = 16;
		public const int CHUNK_TILE_SIZE_Y = 16;
		private Dictionary<Type, ILayer> _layers;

		public World()
		{
			this._layers = new Dictionary<Type, ILayer>();
		}

		public void AddLayer(ILayer layer)
		{
			if (this._layers.ContainsKey(layer.TileEntityType))
				throw new ArgumentException("Cannot register duplicate layer for type \"{0}\"", layer.TileEntityType.Name);
			this._layers.Add(layer.TileEntityType, layer);
		}

		public Layer<T> GetLayerForType<T>() where T : TileEntity
		{
			if (!this._layers.ContainsKey(typeof(T)))
				return null;
			return (Layer<T>) this._layers[typeof(T)];
		}

		public static ChunkCoordinate TileToChunkCoordinate(int tileX, int tileY)
		{
			ChunkCoordinate temp = new ChunkCoordinate(tileX / World.CHUNK_WIDTH_IN_TILES, tileY / World.CHUNK_HEIGHT_IN_TILES);

			// Fix negative coordinates (tile -7 should result in chunk -1, not chunk 0)
			if (tileX < 0)
				temp.ChunkX--;
			if (tileY < 0)
				temp.ChunkY--;

			return temp;
		}
	}
}

