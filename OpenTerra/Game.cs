﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Reflection;
using System.IO;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;
using OpenTK.Platform;
using OpenTerra.Engine.Plugins;

namespace OpenTerra.Engine
{
	class Launcher
	{
		public static void Main(string[] args)
		{
			Console.WriteLine("Launching game...");
			new Game(new Vector2(640, 480)).Run(30, 60);
		}
	}

	public class Game
	{
		private static readonly TimeSpan ONE_SECOND = new TimeSpan(0, 0, 1);
		private static readonly string PLUGIN_DIRECTORY = "Plugins";

		public static Game Instance { get; private set; }

		public GameWindow Window { get; private set; }

		public double TickPeriod { get; private set; }

		public double TicksPerSecond { get; private set; }

		public double FramePeriod { get; private set; }

		public double FramesPerSecond { get; private set; }

		public PluginLoader PluginLoader { get; private set; }

		private bool _shouldRun;

		public Game(Vector2 windowSize)
		{
			GraphicsMode mode = new GraphicsMode(new ColorFormat(32), 24, 0, 8);
			this.Window = new GameWindow((int) windowSize.X, (int) windowSize.Y, mode);
			this.PluginLoader = new PluginLoader(Game.PLUGIN_DIRECTORY);
			if (Game.Instance != null)
			{
				Game.Instance.Stop();
				Game.Instance = null;
			}
			Game.Instance = this;
		}

		#region Public

		public void Run(int tickRate, int maxFrameRate)
		{
			Console.WriteLine("Loading plugins from \"{0}\"...", Path.GetFullPath(Game.PLUGIN_DIRECTORY));
			this.PluginLoader.LoadAllPlugins();

			this._shouldRun = true;

			this.Window.UpdateFrame += OnTick;
			this.Window.RenderFrame += OnRender;
			this.Window.Resize += OnResize;
			this.Window.Load += OnLoad;
			this.Window.VSync = VSyncMode.Off;
			this.Window.Visible = true;
			this.Window.Context.MakeCurrent(null);

			Thread tickThread = new Thread(new ParameterizedThreadStart(TickThread));
			Thread renderThread = new Thread(new ParameterizedThreadStart(RenderThread));
			tickThread.Start(tickRate);
			renderThread.Start(maxFrameRate);

			while (tickThread.IsAlive || renderThread.IsAlive)
			{
				this.Window.Title = string.Format("OpenTerra (FPS: {0} TPS: {1})", this.FramesPerSecond.ToString("0"), this.TicksPerSecond.ToString("0"));
				this.Window.ProcessEvents();
				Thread.Sleep(1);
				if (this.Window.IsExiting)
					this._shouldRun = false;
			}

			this.Window.Exit();
		}

		public void Stop()
		{
			this._shouldRun = false;
		}

		#endregion

		#region Private

		private void TickThread(object tickRate)
		{
			if (!(tickRate is int))
				return;

			long targetTimePerUpdate = (long) ((1.0 / (double) (int) tickRate) * ONE_SECOND.Ticks);
			long tickStart, tickLength = targetTimePerUpdate;
			FrameEventArgs e = new FrameEventArgs();

			while (this._shouldRun)
			{
				tickStart = Util.TicksSinceEpoch();
				this.OnTick(this, e);
				tickLength = Util.TicksSinceEpoch() - tickStart;
				if (tickLength < targetTimePerUpdate)
					Thread.Sleep(new TimeSpan(targetTimePerUpdate - tickLength));
				// Reuse variable and count tick time with sleeping included
				tickLength = Util.TicksSinceEpoch() - tickStart;
				this.TickPeriod = ((double) tickLength) / ONE_SECOND.Ticks;
				this.TicksPerSecond = 1.0 / this.TickPeriod;
			}
		}

		private void RenderThread(object maxFrameRate)
		{
			if (!(maxFrameRate is int))
				return;

			long targetTimePerFrame = (long) ((1.0 / (double) (int) maxFrameRate) * ONE_SECOND.Ticks);
			long frameStart, frameLength = targetTimePerFrame;
			FrameEventArgs e = new FrameEventArgs();
			this.Window.MakeCurrent();

			while (this._shouldRun)
			{
				frameStart = Util.TicksSinceEpoch();
				this.OnRender(this, e);
				this.Window.SwapBuffers();
				frameLength = Util.TicksSinceEpoch() - frameStart;
				if (frameLength <= targetTimePerFrame)
					Thread.Sleep(new TimeSpan(targetTimePerFrame - frameLength));
				// Reuse variable and count frame time with sleeping included (for FPS measurement)
				frameLength = Util.TicksSinceEpoch() - frameStart;
				this.FramePeriod = ((double) frameLength) / (double) ONE_SECOND.Ticks;
				this.FramesPerSecond = 1.0 / this.FramePeriod;
			}
		}

		private void OnLoad(object sender, EventArgs e)
		{
			GL.ClearColor(0.0f, 0.0f, 0.0f, 1.0f);
			OnResize(null, null);
		}

		private void OnResize(object sender, EventArgs e)
		{
			GL.Viewport(0, 0, this.Window.Width, this.Window.Height);
		}

		private void OnTick(object sender, FrameEventArgs e)
		{
			// Tick here

			if (this.Window.Keyboard[Key.Escape])
				this.Stop();
		}

		private void OnRender(object sender, FrameEventArgs e)
		{
			GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

			GL.MatrixMode(MatrixMode.Projection);
			GL.LoadIdentity();
			GL.Ortho(0, this.Window.Width, this.Window.Height, 0, 0.0, 10.0);
			GL.MatrixMode(MatrixMode.Modelview);
			GL.LoadIdentity();

			// Render here
		}

		#endregion
	}
}
