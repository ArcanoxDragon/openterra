﻿using System;

namespace OpenTerra.Engine
{
	/// <summary>
	/// A tile entity represents extra data or behaviors associated with a Tile
	/// </summary>
	public class TileEntity
	{
		public Tile Parent { get; private set; }

		public TileEntity(Tile parent)
		{
			this.Parent = parent;
		}
	}

	/// <summary>
	/// A tile represents one space on a world/layer/chunk.
	/// </summary>
	public struct Tile
	{
		public uint TileID { get; set; }

		public int TileLocationX { get; set; }

		public int TileLocationY { get; set; }

		public TileEntity Entity { get; set; }

		public Tile(uint tileID, int tileX, int tileY) : this()
		{
			this.TileID = tileID;
			this.TileLocationX = tileX;
			this.TileLocationY = tileY;
			this.Entity = null;
		}
	}
}

