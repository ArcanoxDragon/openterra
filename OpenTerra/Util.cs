﻿using System;

namespace OpenTerra.Engine
{
	public static class Util
	{
		public static readonly DateTime EPOCH = new DateTime(1970, 1, 1);

		public static double MillisSinceEpoch()
		{
			return (DateTime.Now - EPOCH).TotalMilliseconds;
		}

		public static long TicksSinceEpoch()
		{
			return (DateTime.Now - EPOCH).Ticks;
		}
	}
}

