﻿using System;

using System.Collections.Generic;
using System.Reflection;
using System.IO;

namespace OpenTerra.Engine.Plugins
{
	public class PluginLoader
	{
		private string _rootPath;
		private Dictionary<string, Plugin> _loadedPlugins;

		public PluginLoader(string pluginsPath)
		{
			this._rootPath = pluginsPath;
			this._loadedPlugins = new Dictionary<string, Plugin>();
		}

		public List<Plugin> LoadedPlugins
		{
			get
			{
				return new List<Plugin>(this._loadedPlugins.Values);
			}
		}

		public Plugin GetPlugin(string pluginID)
		{
			if (!this._loadedPlugins.ContainsKey(pluginID))
				return null;
			return this._loadedPlugins[pluginID];
		}

		public void LoadAllPlugins()
		{
			if (this._loadedPlugins.Count > 0)
			{
				Console.WriteLine("LoadAllPlugins called while plugins were already loaded!");
				return;
			}

			string fullPath = Path.GetFullPath(Path.Combine(Directory.GetCurrentDirectory(), this._rootPath));

			if (!Directory.Exists(fullPath))
			{
				Directory.CreateDirectory(fullPath);
				Console.WriteLine("Plugins directory nonexistent.\nCreated new plugins directory at \"{0}\"...\nInstall some plugins and restart game.", fullPath);
				return;
			}

			foreach (string file in Directory.EnumerateFiles(fullPath))
			{
				if (Path.GetExtension(file) == "dll")
				{
					Assembly a = Assembly.LoadFile(file);
					if (a == null)
					{
						Console.WriteLine("Plugin \"{0}\" does not appear to be a valid assembly", Path.GetFileNameWithoutExtension(file));
						continue;
					}

					foreach (Module m in a.GetModules())
					{
						foreach (Type t in m.GetTypes())
						{
							if (t.IsSubclassOf(typeof(Plugin)))
							{
								Plugin p = (Plugin) a.CreateInstance(t.FullName);
								if (p != null)
								{
									string pluginID = null;
									foreach (object obj in p.GetType().GetCustomAttributes(true))
									{
										if (obj is PluginAttribute)
										{
											pluginID = ((PluginAttribute) obj).PluginId;
										}
									}
									if (pluginID == null)
									{
										Console.WriteLine("Plugin \"{0}\" is missing a PluginAttribute declaration", t.Name);
									}
									else
									{
										Console.WriteLine("Plugin \"{0}\" was loaded successfully", p.Name);
										this._loadedPlugins.Add(pluginID, p);
									}
								}
							}
						}
					}
				}
			}
		}
	}
}

