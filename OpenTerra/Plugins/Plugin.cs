﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Reflection;

namespace OpenTerra.Engine.Plugins
{
	[AttributeUsage(AttributeTargets.Class)]
	public class PluginAttribute : Attribute
	{
		public readonly string PluginId;

		public PluginAttribute(string pluginID)
		{
			if (pluginID.Contains(" "))
				throw new ArgumentException("Plugin ID cannot contain spaces", "pluginID");
			this.PluginId = pluginID;
		}
	}

	public abstract class Plugin
	{
		private Game _parent;

		public Plugin()
		{
			this._parent = null;
		}

		public abstract string Name { get; }

		public virtual void LoadPlugin(Game parent)
		{
			this._parent = parent;
		}
	}
}

